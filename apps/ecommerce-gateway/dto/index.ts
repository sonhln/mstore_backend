'use strict';

import { ApiResponseProperty } from '@nestjs/swagger';

export class AccountResponse {
    @ApiResponseProperty({ example: 1 })
    id: number;

    @ApiResponseProperty({ example: null })
    username?: string;

    @ApiResponseProperty({ example: null })
    email?: string;

    @ApiResponseProperty({ example: 2 })
    role: number;
}

export class TokenPayloadDto {
    expiresIn: number;
    accessToken: string;
    cdnToken: string;
    email: string;

    constructor(payload: Partial<TokenPayloadDto>) {
        Object.assign(this, payload);
    }
}
