import { AccountStatus } from '@app/common';
import { LoginEmailAccountBody, RegisterEmailAccountBody } from '@app/common/dto/AccountDto';
import { ConfigService } from '@app/config';
import { MainRepo } from '@app/repositories/main';
import { CachingService } from '@libs/caching';
import { UtilsService } from '@libs/utils';
import {
    Inject,
    Injectable,
    Logger,
    NotFoundException,
    UnauthorizedException,
    UnprocessableEntityException
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ClientProxy } from '@nestjs/microservices';
import { Prisma } from '@prisma/client';
import { TokenPayloadDto } from 'apps/ecommerce-gateway/dto';

@Injectable()
export class AccountService {
    private readonly _logger = new Logger(AccountService.name);

    constructor(
        private readonly _jwtService: JwtService,
        private readonly _utils: UtilsService,
        private readonly _mainRepo: MainRepo,
        private readonly _caching: CachingService
    ) {}

    async registerAccountEmail(body: RegisterEmailAccountBody) {
        const { email, password } = body;

        const isEmailExist = await this._mainRepo.account.findFirst({ where: { email } });
        if (isEmailExist) {
            throw new UnprocessableEntityException('EMAIL_ALREADY_EXISTS');
        }

        const createdAccount = await this._mainRepo.account.create({
            data: {
                email,
                fullName: '',
                password: this._utils.hashValue(password),
                status: AccountStatus.NEW
            },
            select: { id: true, email: true }
        });

        return createdAccount;
    }

    async loginAccountEmail(body: LoginEmailAccountBody) {
        const { email, password } = body;
        const account = await this._mainRepo.account.findUnique({
            where: { email },
            select: { id: true, password: true, email: true, status: true }
        });

        if (!account || !this._utils.compareHash(password, account.password)) {
            throw new UnauthorizedException('INVALID_CREDENTIALS');
        }

        const payload = { sub: this._utils.generateUniqueId(), id: account.id };

        return new TokenPayloadDto({
            accessToken: this._jwtService.sign(payload),
            cdnToken: this._jwtService.sign(payload, { secret: ConfigService.getInstance().get('JWT_CDN_SECRET_KEY') }),
            expiresIn: ConfigService.getInstance().getNumber('JWT_MOBILE_EXPIRATION_TIME')
        });
    }

    async getMyAccount(id: number) {
        const user = await this._mainRepo.account.findUnique({ where: { id }, select: this.getAccountSelect() });
        if (!user) throw new NotFoundException();
        return user;
    }

    private getAccountSelect(): Prisma.AccountSelect {
        return {
            id: true,
            email: true,
            phone: true,
            fullName: true,
            avatar: true,
            status: true
        };
    }
}
