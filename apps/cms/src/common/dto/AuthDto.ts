import { ResponseDto } from '@app/common/dto/RequestDto';
import { ApiProperty, ApiResponseProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class LoginBody {
    @ApiProperty({ example: 'superadmin@livetrade.vn' })
    @IsNotEmpty()
    @IsString()
    email: string;

    @ApiProperty({ example: 'livetrade.vn' })
    @IsNotEmpty()
    @IsString()
    password: string;
}

export class LoginResponse {
    @ApiResponseProperty({
        example:
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNjI0MDA5MzU0LCJleHAiOjE3MjQwMDkzNTN9.8c_BfTzjNG71_c2imVIMkvqkioNorugf-rlXcE-t7_s'
    })
    accessToken: string;

    @ApiResponseProperty({ example: 999999999 })
    expiresIn: number;

    @ApiResponseProperty({ example: 'superadmin@livetrade.vn' })
    email: string;

    @ApiResponseProperty({ example: { id: 2, name: 'Administrator' } })
    role: any;
}

export class LoginResponseDto extends ResponseDto {
    @ApiResponseProperty({ type: LoginResponse })
    data: any;
}
