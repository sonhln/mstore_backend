import { MainRepoModule } from '@app/repositories';
import { UtilsModule } from '@libs/utils';
import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
    imports: [UtilsModule, MainRepoModule],
    controllers: [UserController],
    providers: [UserService],
    exports: [UserService]
})
export class UserModule {}
