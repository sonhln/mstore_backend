import { ConfigService } from '@app/config';
import { MainRepoModule } from '@app/repositories';
import { CachingModule } from '@libs/caching';
import { UtilsModule } from '@libs/utils';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

@Module({
    imports: [
        PassportModule.register({ defaultStrategy: ['jwt'] }),
        JwtModule.registerAsync({ useFactory: () => ConfigService.getInstance().getCMSJwtConfig() }),
        MainRepoModule,
        UtilsModule,
        CachingModule
    ],
    controllers: [AuthController],
    providers: [AuthService]
})
export class AuthModule {}
