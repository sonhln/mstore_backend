import { ConfigService } from '@app/config';
import { MainRepo } from '@app/repositories';
import { CachingService } from '@libs/caching';
import { UtilsService } from '@libs/utils';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { TokenPayloadDto } from 'apps/ecommerce-gateway/dto';
import { compareSync } from 'bcryptjs';
import { LoginBody } from '../common/dto/AuthDto';

@Injectable()
export class AuthService {
    constructor(
        private readonly _jwtService: JwtService,
        private readonly _mainRepo: MainRepo,
        private readonly _utils: UtilsService,
        private readonly _caching: CachingService
    ) {}

    async login(body: LoginBody) {
        const { email, password } = body;
        const user = await this._mainRepo.user.findFirst({
            where: { email },
            select: { id: true, password: true, email: true, status: true }
        });

        if (!user || !compareSync(password, user.password)) {
            throw new UnauthorizedException('INVALID_CREDENTIALS');
        }

        // if (user.status === STATUS.INACTIVE) {
        //     throw new UnauthorizedException('LOCKED');
        // }

        const payload = { sub: this._utils.generateUniqueId(), id: user.id };

        const tokenPayload = new TokenPayloadDto({
            accessToken: this._jwtService.sign(payload),
            cdnToken: this._jwtService.sign(payload, { secret: ConfigService.getInstance().get('JWT_CDN_SECRET_KEY') }),
            expiresIn: ConfigService.getInstance().getNumber('JWT_CMS_EXPIRATION_TIME'),
            email: user.email
        });

        console.log('tokenPayload', tokenPayload);

        return tokenPayload;
    }
}
