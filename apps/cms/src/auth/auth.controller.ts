import { VERSIONING } from '@app/common';
import { Public } from '@app/common/decorators/public.decorator';
import { BadRequestExceptionFilter } from '@app/common/filters/bad-request.filter';
import { GatewayHttpExceptionFilter } from '@app/common/filters/gateway-http-exception.filter';
import { TransformResponseInterceptor } from '@app/common/interceptors/transform-response.interceptor';
import { getMessage } from '@app/common/messages';
import { MainValidationPipe } from '@app/common/pipes/validation.pipe';
import {
    Body,
    Controller,
    HttpCode,
    HttpStatus,
    Logger,
    Post,
    UseFilters,
    UseGuards,
    UseInterceptors,
    UsePipes
} from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { LoginBody, LoginResponseDto } from '../common/dto/AuthDto';
import { AuthService } from './auth.service';

@UseFilters(GatewayHttpExceptionFilter)
@UseFilters(BadRequestExceptionFilter)
@UseInterceptors(TransformResponseInterceptor)
@Controller({ version: VERSIONING.V1, path: 'auth' })
export class AuthController {
    private readonly _logger = new Logger(AuthController.name);

    constructor(private readonly _service: AuthService) {}

    @Public()
    @HttpCode(HttpStatus.OK)
    @ApiTags('Auth')
    @ApiOperation({ summary: 'Sign in to the CMS system' })
    @ApiOkResponse({ description: getMessage('GENERAL.SUCCESS'), type: LoginResponseDto })
    @UsePipes(new MainValidationPipe())
    @Post('login')
    async login(@Body() body: LoginBody) {
        this._logger.log('login');
        return this._service.login(body);
    }
}
