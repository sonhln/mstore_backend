import { ConfigService } from '@app/config';
import { Logger, VersioningType } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { json, urlencoded } from 'body-parser';
import { useContainer } from 'class-validator';
import { AppModule } from './app.module';

const logger = new Logger('MStore-Main');

const cmsConfig = {
    host: ConfigService.getInstance().get('CMS_HOST'),
    port: ConfigService.getInstance().get('CMS_PORT')
};

async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppModule);

    app.enableVersioning({ type: VersioningType.URI });
    app.setGlobalPrefix('/api');

    app.use(json());
    app.use(urlencoded({ extended: true }));
    app.enable('trust proxy'); // only if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)
    app.enableCors({ origin: '*', allowedHeaders: '*' });

    useContainer(app.select(AppModule), { fallbackOnErrors: true });

    if (['development', 'staging'].includes(ConfigService.getInstance().nodeEnv)) {
        const { DocumentBuilder, SwaggerModule } = await import('@nestjs/swagger');
        const { version } = await import('../../../package.json');

        const options = new DocumentBuilder()
            .setTitle('MStore CMS API')
            .setDescription('CAUTION: This document is for internal use only')
            .setVersion(version)
            .addBearerAuth()
            .build();

        const document = SwaggerModule.createDocument(app, options);

        SwaggerModule.setup('documentation', app, document);
    }

    await app.listen(cmsConfig.port, () => logger.log(`MStore-CMS is running at http://localhost:${cmsConfig.port}`));
}

bootstrap();
