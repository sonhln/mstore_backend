-- CreateTable
CREATE TABLE "user" (
    "id" SERIAL NOT NULL,
    "full_name" VARCHAR(255),
    "email" VARCHAR(320) NOT NULL,
    "phone" VARCHAR(20),
    "password" VARCHAR(255) NOT NULL,
    "status" SMALLINT NOT NULL DEFAULT 1,
    "avatar" VARCHAR(255),
    "gender" BOOLEAN NOT NULL DEFAULT false,
    "date_of_birth" TIMESTAMP(3),
    "id_card_number" VARCHAR(255),
    "address" VARCHAR(320),
    "is_super" BOOLEAN NOT NULL DEFAULT false,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "user_email_key" ON "user"("email");

INSERT INTO "user"("full_name", "email", "password", "is_super", "created_at", "updated_at")
VALUES
('Super Admin', 'superadmin@livetrade.vn', '$2a$10$oTpekm59ZvUIh6/u9KE20evknk3FrqVKZ8q8vHDXW6xrS2WqKacmO', true, NOW(), NOW());
