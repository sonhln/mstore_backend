/*
  Warnings:

  - You are about to drop the column `first_name` on the `account` table. All the data in the column will be lost.
  - You are about to drop the column `last_name` on the `account` table. All the data in the column will be lost.
  - You are about to alter the column `full_name` on the `account` table. The data in that column could be lost. The data in that column will be cast from `VarChar(511)` to `VarChar(255)`.
  - A unique constraint covering the columns `[account_seller_id]` on the table `account` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE "account" DROP COLUMN "first_name",
DROP COLUMN "last_name",
ADD COLUMN     "account_seller_id" INTEGER,
ALTER COLUMN "full_name" SET DATA TYPE VARCHAR(255);

-- CreateTable
CREATE TABLE "account_seller" (
    "id" SERIAL NOT NULL,
    "full_name" VARCHAR(255),
    "avatar" VARCHAR(255),
    "phone" VARCHAR(10),
    "address" VARCHAR(255),
    "city" VARCHAR(255),
    "district" VARCHAR(255),
    "ward" VARCHAR(255),
    "email" VARCHAR(20),
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "deleted_at" TIMESTAMP(3),

    CONSTRAINT "account_seller_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "transport_method" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(320) NOT NULL,
    "min_weight" SMALLINT,
    "min_size" SMALLINT,
    "max_weight" SMALLINT,
    "max_size" SMALLINT,
    "max_price" SMALLINT,
    "note" VARCHAR(255),
    "supportNumber" VARCHAR(255),
    "activity_time_range" VARCHAR(255),
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "deleted_at" TIMESTAMP(3),

    CONSTRAINT "transport_method_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "seller_transport" (
    "account_seller_id" INTEGER NOT NULL,
    "transport_method_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "seller_transport_pkey" PRIMARY KEY ("account_seller_id","transport_method_id")
);

-- CreateIndex
CREATE UNIQUE INDEX "transport_method_name_key" ON "transport_method"("name");

-- CreateIndex
CREATE UNIQUE INDEX "seller_transport_account_seller_id_key" ON "seller_transport"("account_seller_id");

-- CreateIndex
CREATE UNIQUE INDEX "seller_transport_transport_method_id_key" ON "seller_transport"("transport_method_id");

-- CreateIndex
CREATE UNIQUE INDEX "account_account_seller_id_key" ON "account"("account_seller_id");

-- AddForeignKey
ALTER TABLE "account" ADD CONSTRAINT "account_account_seller_id_fkey" FOREIGN KEY ("account_seller_id") REFERENCES "account_seller"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "seller_transport" ADD CONSTRAINT "seller_transport_account_seller_id_fkey" FOREIGN KEY ("account_seller_id") REFERENCES "account_seller"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "seller_transport" ADD CONSTRAINT "seller_transport_transport_method_id_fkey" FOREIGN KEY ("transport_method_id") REFERENCES "transport_method"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
