import { ApiProperty, ApiPropertyOptional, ApiResponseProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, MaxLength, MinLength } from 'class-validator';
import { VALIDATION } from '../contants';
import { ResponseDto } from './RequestDto';

export class RegisterEmailAccountBody {
    @ApiProperty({ example: 'nguyenvana@gmail.com' })
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @ApiProperty({ example: '123456' })
    @IsNotEmpty()
    @MinLength(VALIDATION.PASSWORD.MIN_LENGTH)
    @MaxLength(VALIDATION.PASSWORD.MAX_LENGTH)
    password: string;
}

export class RegisterAccountEmailResponse {
    @ApiResponseProperty({ example: 2 })
    id: number;

    @ApiResponseProperty({ example: 'nguyenvanb@gmail.com' })
    email: string;
}

export class RegisterAccountEmailResponseDto extends ResponseDto {
    @ApiResponseProperty({ type: RegisterAccountEmailResponse })
    data: RegisterAccountEmailResponse;
}

export class LoginEmailAccountBody {
    @ApiProperty({ example: 'nguyenvana@gmail.com' })
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @ApiProperty({ example: '123456' })
    @IsNotEmpty()
    @MinLength(VALIDATION.PASSWORD.MIN_LENGTH)
    @MaxLength(VALIDATION.PASSWORD.MAX_LENGTH)
    password: string;
}

export class LoginEmailResponse {
    @ApiResponseProperty({
        example:
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNjI4MjQ1MDEzLCJleHAiOjI2MjgyNDUwMTJ9.KifuQk_2QiftuiAieNHASbdWm4uEVSkD3ec3ubmKVSE'
    })
    accessToken: string;

    @ApiResponseProperty({ example: 3600 })
    expiresIn: number;
}

export class LoginEmailResponseDto extends ResponseDto {
    @ApiResponseProperty({ type: LoginEmailResponse })
    data: LoginEmailResponse;
}

export class AccountDetailResponse {
    @ApiResponseProperty({ example: 12 })
    id: number;

    @ApiResponseProperty({ example: 'BchHu70@yahoo.com' })
    email: string;

    @ApiResponseProperty({ example: 'Vân Khánh' })
    firstName: string;

    @ApiResponseProperty({ example: 'Hà' })
    lastName: string;

    @ApiResponseProperty({ example: 'https://cdn.fakercloud.com/avatars/edhenderson_128.jpg' })
    avatar: string;

    @ApiResponseProperty({ example: '3626341389' })
    phone: string;

    // @ApiResponseProperty({ example: 1 })
    // status: number;
}

export class AccountDetailResponseDto extends ResponseDto {
    @ApiResponseProperty({ type: AccountDetailResponse })
    data: AccountDetailResponse;
}
